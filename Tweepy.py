import tweepy
import pandas as pd
import pyodbc

CONSUMER_KEY = "sq40y7BAvdMu8xUtkJcRpESna"
CONSUMER_SECRET = "2sZAsrsYs2vDGuZY0j92Xmun2ZCCtKkV3DcKuTklOVk24h7erC"
ACCESS_TOKEN = "3096459564-5U7qiCxyfK4pjAwhPIAeXzI3gUJeeUrBKFnU3D8"
ACCESS_TOKEN_SECRET = "vaH0RvqudPVKjzS4MRpegWqbv1cykszMyoDAlXPljov30"

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
api = tweepy.API(auth, wait_on_rate_limit=True)

server = 'hinsdatabase.database.windows.net'
database = 'GenHK-Twitter'
username = 'hins'
password = 'Aa123456'   
driver= '{ODBC Driver 17 for SQL Server}'

print(">>>Welcome To Tweeter Data Crawler<<< \n")

User_name = input("Which twitter user's profile information you want to know?\n(Dafault is JoeBiden) ") or "JoeBiden"
user = api.get_user(User_name)
print(f"You want to know about {User_name}'s profile information\n")

User_info_list = [[user.id, user.name, user.description, user.location, user.followers_count, user.created_at]]
Info_df = pd.DataFrame(data = User_info_list, columns=["User_ID", "User_Name", "User_Description", "User_Location",  "User_Follower_Count", "User_Created_At"])
print(f"{Info_df}\n")

server = 'hinsdatabase.database.windows.net'
database = 'GenHK-Twitter'
username = 'hins'
password = 'Aa123456'   
driver= '{ODBC Driver 17 for SQL Server}'
cnxn = pyodbc.connect('DRIVER=' + driver +';SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password, charset="UTF-8")
cursor = cnxn.cursor()

insert_info = str(input(f"Do you want to insert {User_name}'s information into database?(y/n) "))

while insert_info != "y" or "n":
    if insert_info == str("y"):
        try:
            print("\nLoading...\n")
            # Insert Dataframe into SQL Server:
            for index, row  in Info_df.iterrows():
                cursor.execute("INSERT INTO Information (User_ID, User_Name, User_Description, User_Location, User_Follower_Count, User_Created_At) values(?,?,?,?,?,?)", row.User_ID, row.User_Name, row.User_Description, row.User_Location, row.User_Follower_Count, row.User_Created_At)
            cnxn.commit()
        except:
            # UPDATE Dataframe into SQL Server:
            for index, row  in Info_df.iterrows():
                cursor.execute("UPDATE Information set User_Name = (?), User_Description = (?), User_Location = (?), User_Follower_Count = (?), User_Created_At = (?) where User_ID = (?)", row.User_Name, row.User_Description, row.User_Location, row.User_Follower_Count, row.User_Created_At, row.User_ID)
            cnxn.commit()
        print(f"Insert {User_name}'s information into database successfully !")
        break
    elif insert_info ==str("n"):
        print(f"Didn't insert {User_name}'s information into database!")
        break
    else:
        print("Wrong input! Please try again!")
        insert_info = str(input(f"Do you want to insert {User_name}'s information to database?(y/n) "))

print("========================================================================================================================")

print(f"Now you can show the followers name from {user.name}\n")

Collect_follower_sum = int(input(f"How many followers name you want to show from {user.name}? "))
print(f"You want to show {Collect_follower_sum} followers name from {user.name}\n")
print("Loading...\n")

followers = tweepy.Cursor(api.followers, User_name).items(Collect_follower_sum)
followers_list = [[follower.id, follower.name, follower.created_at] for follower in followers]
followers_df = pd.DataFrame(data = followers_list, columns=["User_ID", "User_Name", "Created_At"])
print(f"{followers_df}\n")

insert_follower = str(input("Do you want to insert followers data into database?(y/n) "))

while insert_follower != "y" or "n":
    if insert_follower == str("y"):
        try:
            print("\nLoading...\n")
            # Insert Dataframe into SQL Server:
            for index, row  in followers_df.iterrows():
                cursor.execute("INSERT INTO Follower (User_ID,User_Name,Created_At) values(?,?,?)", row.User_ID, row.User_Name, row.Created_At)
            cnxn.commit()
        except:
            # UPDATE Dataframe into SQL Server:
            for index, row  in followers_df.iterrows():
                cursor.execute("UPDATE Follower set User_Name = (?), Created_At = (?) where User_ID = (?)", row.User_Name, row.Created_At, row.User_ID)
            cnxn.commit()

        print("Insert followers data into database successfully!")
        break
    elif insert_follower ==str("n"):
        print("Didn't insert followers data into database!")
        break
    else:
        print("Wrong input! Please try again!")
        insert_follower = str(input("Do you want to insert followers data to database?(y/n) "))

print("========================================================================================================================")

print("Now you can search tweets by yours keyword\n")

search_words = input("What Keyword you want to search?\n(Default is Coronavirus + Vaccination) ") or "Coronavirus + Vaccination"
Collect_tweet_sum = int(input("How many tweets you want to show? "))
print(f"You want to show {Collect_tweet_sum} tweets by keywords {search_words}\n")

tweets = tweepy.Cursor(api.search,
              q=search_words,
              lang="en",).items(Collect_tweet_sum)
users_locs  = []
for tweet in tweets:
    Tweet_id = tweet.id
    User_ID = tweet.user.id
    User_name = tweet.user.screen_name
    User_location = tweet.user.location
    Text = tweet.text
    Created_At = tweet.created_at
    Tweet_link = ("https://twitter.com/twitter/statuses/" +str(Tweet_id))  
    users_locs.append([Tweet_id, User_ID, User_name, User_location, Text, Created_At , Tweet_link]) 

tweet_text = pd.DataFrame(data=users_locs, 
 columns=['Tweet_ID','User_ID', 'User_name','User_location','Text','Created_At','Tweet_link'])
print(f"{tweet_text}\n")

insert_tweet = str(input("Do you want to insert tweets data into database?(y/n) "))

while insert_tweet != "y" or "n":

    if insert_tweet == str("y"):
        try:
            print("\nLoading...\n")
            # Insert Dataframe into SQL Server:
            for index, row  in tweet_text.iterrows():
                cursor.execute("INSERT INTO Tweet (Tweet_ID, User_ID, User_name, User_location, Text, Created_At, Tweet_link) values(?,?,?,?,?,?,?)", row.Tweet_ID, row.User_ID, row.User_name, row.User_location, row.Text, row.Created_At, row.Tweet_link)
            cnxn.commit()
        except:
            # UPDATE Dataframe into SQL Server:
            for index, row  in tweet_text.iterrows():
                cursor.execute("UPDATE Follower set User_ID = (?), User_name = (?), User_location = (?), Text = (?), Created_At = (?), Tweet_link(?)  where Tweet_ID = (?)", row.User_ID, row.User_name, row.User_location, row.Text, row.Created_At, row.Tweet_link, row.Tweet_ID)
            cnxn.commit()
        print("Insert tweets data into database successfully!")
        break
    elif insert_tweet ==str("n"):
        print("Didn't Insert tweets data into database!")
        break
    else:
        print("Wrong input! Please try again!")
        insert_tweet = str(input("Do you want to insert tweets data to database?(y/n) "))

print("========================================================================================================================")
print(">>>The end, Thank you!<<<")
a = input('Press Any Key To Exit')
if a:
    exit(0)
